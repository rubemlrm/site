---
layout: post
status: publish
published: true
title: Sysadmin-toolbox
author:
  display_name: Rubem Mota
  login: Rubemlrm
  email: rubemmota89@gmail.com
  url: http://rubemlrm.com
author_login: Rubemlrm
author_email: rubemmota89@gmail.com
author_url: http://rubemlrm.com
excerpt: Depois de algum tempo a programar em perl , decidi "que tal por as minhas
  capacidades a prova" e&nbsp;construi&nbsp;um painel&nbsp;administrativo&nbsp;em
  perl para gerir sistemas.
wordpress_id: 214
wordpress_url: http://rubemlrm.com/?p=214
date: '2012-02-16 15:25:13 +0000'
date_gmt: '2012-02-16 15:25:13 +0000'
categories: []
tags: []
comments: []
---
<p>Depois de algum tempo a programar em perl , decidi "que tal por as minhas capacidades a prova" e construi um painel administrativo em perl para gerir sistemas.<a id="more"></a><a id="more-214"></a>E depois de algumas horas a programar , tenho uma versão 1 pronta a utilizar ainda com poucas funcionalidades é certo , mas para já considero que tem as funcionalidades básicas já implementadas , tais como:</p>
<p>-Adicionar/remover utilizadores;</p>
<p>-Possibilidade de fazer backups dos dados de um utilizador que se vai apagar;</p>
<p>-Gestão de crons;</p>
<p>-Funcionalidade para testar os pcs da rede.De forma a ver quais estão online e quais não estão;</p>
<p>-Funcionalidade para encontrar pastas vazias e ficar com um registo das mesmas;</p>
<p>-Funcionalidade para ver quais são os processos que estão a gastar mais recursos</p>
<p>-Funcionalidade para adicionar vários utilizadores a uma máquina de forma automática</p>
<p>-Funcionalidade para encontrar ficheiros que tenham um tamanho superior ao indicado.</p>
<p> </p>
<p>Este projecto está disponível no github a partir do seguinte link : <a href="https://github.com/Rubemlrm/sysadmin-toolbox">https://github.com/Rubemlrm/sysadmin-toolbox</a> .</p>
<p> </p>
<p> </p>
<p>Para utilizar basta fazer download do projecto e configurar o ficheiro includes.pl que está na pasta includes , com o caminho correcto para o script.Espero que reportem possiveis bugs e sugestões =).</p>
